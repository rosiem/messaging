import React from 'react';
import renderer from 'react-test-renderer';
import MessageForm from './Form';

describe('MessageForm component', () => {
  it('renders correctly', () => {
    const rendered = renderer.create(
      <MessageForm placeholder="Test" onSubmit={() => {}} />
    );
    expect(rendered.toJSON()).toMatchSnapshot();
  });
});