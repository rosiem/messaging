import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input } from '../styles';

class Form extends Component {
  static propTypes = {
    placeholder: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired,
  }

  state = {value: ''};

  clearInputValue = () =>
    this.setState({
      value: ''
    })

  handleChange = (event) => {
    this.setState({value: event.target.value});
  }

  handleSubmit = (event) => {
    this.props.onSubmit(this.state.value);
    this.clearInputValue()
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          <Input placeholder={this.props.placeholder} type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
      </form>
    );
  }
}

export default Form;