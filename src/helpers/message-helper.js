import random from 'lodash.random';

export const createMessage = (message) => ({
  ...message,
  id: random(10000)
});

export const removeLastMessage = (messages, type) => {
  const messageToBeRemoved = [...messages].reverse().find((message) => message.messageType === type);
  if(messageToBeRemoved) {
    return messages.filter(message => messageToBeRemoved.id !== message.id);
  } else {
    return messages;
  }
};