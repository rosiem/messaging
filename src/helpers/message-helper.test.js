import { removeLastMessage, createMessage } from './message-helper';

describe('removeLastMessage', () => {
  const messages = [
    createMessage('foo', 'local'),
    createMessage('bar', 'external'),
    createMessage('test', 'local'),
  ]

  it('should remove the last message with matching message type', () => {
    let messageType = 'external';
    const withoutLastExternalMessage = removeLastMessage(messages, messageType)
    expect(withoutLastExternalMessage.find(message => message.messageType === messageType)).toBe(undefined);

    messageType = 'local';
    const withoutLastLocalMessage = removeLastMessage(withoutLastExternalMessage, messageType);
    expect(withoutLastLocalMessage.length).toBe(1);
    expect(withoutLastLocalMessage[0].content).toEqual('foo');
  });

  it('should do nothing if there is no matching message type', () => {
    let messageType = 'external';
    let withoutLastLocalMessage = removeLastMessage(messages, messageType)
    expect(withoutLastLocalMessage.length).toBe(2);

    withoutLastLocalMessage = removeLastMessage(messages, messageType)
    expect(withoutLastLocalMessage.length).toBe(2);

  });
})