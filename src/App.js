import React, { Component } from 'react';
import Form from './components/Form';
import {
  subscribeToChatMessage,
  subscribeToRemoveLastMessage,
  subscribeToNickname,
  handleMessage
} from './api';
import { createMessage, removeLastMessage } from './helpers/message-helper';
import { Bubble, MessageContainer, AppContainer, Header } from './styles';

class App extends Component {
  constructor(props) {
    super(props);
    subscribeToChatMessage((err, message) =>
      this.setState(state => ({
        messages: [...state.messages, createMessage(message)]
      })
    ));

    subscribeToRemoveLastMessage((errr, message) =>
      this.setState(state => ({
        messages: removeLastMessage(state.messages, message.type)
      })
    ));

    subscribeToNickname((err, {nick}) => {
      this.setState({
        nick
      })
    })
  }

  state = {
    messages: [],
    externalMessages: []
  };

  render() {
    return (
      <AppContainer className="app">
        <Header>{ this.state.nick && <span>You are chatting to {this.state.nick}</span> }</Header>
        <Form onSubmit={handleMessage} placeholder="Enter a message"/>
        <MessageContainer>
        { this.state.messages.map(message => (
          <Bubble contentType={message.contentType} messageType={message.messageType} key={message.id}>{message.content}</Bubble>
        )) }
        </MessageContainer>
      </AppContainer>
    );
  }
}

export default App;
