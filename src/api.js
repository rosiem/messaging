import openSocket from 'socket.io-client';
const  socket = openSocket('http://localhost:8000');

function subscribeToChatMessage(cb) {
  socket.on('chat message', message => cb(null, message));
}

function subscribeToRemoveLastMessage(cb) {
  socket.on('remove last message', message => cb(null, message));
}

function subscribeToNickname(cb) {
  socket.on('nickname added', nick => cb(null, nick));
}

function submitChatMessage(val) {
  socket.emit('chat message', val)
}

function addNickname(nick) {
  socket.emit('add nickname', nick)
}

function removeLastMessage() {
  socket.emit('remove last message')
}

function submitThoughtMessage(val) {
  socket.emit('thought message', val)
}

function handleMessage(msg) {
  const messageArr = msg.split(" ");
  const messageCommand = messageArr[0];

  if (messageCommand === "/nick") {
    addNickname(messageArr[1]);
  } else if (messageCommand === "/oops") {
    removeLastMessage();
  } else if (messageCommand === "/think") {
    submitThoughtMessage(messageArr[1]);
  } else {
    submitChatMessage(msg)
  }
}

export {
  subscribeToChatMessage,
  subscribeToRemoveLastMessage,
  subscribeToNickname,
  handleMessage
};
