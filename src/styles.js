import styled, { css } from 'styled-components';
import { LOCAL_MESSAGE_TYPE , EXTERNAL_MESSAGE_TYPE, THOUGHT_MESSAGE } from './helpers/constants';

export const Bubble = styled.div`
	margin-bottom: 10px; 
	position:relative;
	padding:10px 20px;
	border-radius:25px;
	color: black;
  
  ${props => props.messageType === EXTERNAL_MESSAGE_TYPE && css`
		background:#E5E5EA;
		float: left;
		margin-right: 100px;
		
    &:before {
      content:"";
      position:absolute;
      z-index:2;
      bottom:-2px;
      left:-7px;
      height:20px;
      border-left:20px solid #E5E5EA;
      border-bottom-right-radius: 16px 14px;
      -webkit-transform:translate(0, -2px);
    }
  
    &:after {
      content:"";
      position:absolute;
      z-index:3;
      bottom:-2px;
      left:4px;
      width:26px;
      height:20px;
      background:white;
      border-bottom-right-radius: 10px;
      -webkit-transform:translate(-30px, -2px);
    }
    
    ${props => props.contentType === THOUGHT_MESSAGE && css`
			color: grey
		`}
	`}
	
	${props => props.messageType === LOCAL_MESSAGE_TYPE && css`
    background: yellowgreen;
    float: right;
    margin-left: 100px;
      
    &:before {
      content:"";
      position:absolute;
      z-index:-1;
      bottom:-2px;
      right:-7px;
      height:20px;
      border-right:20px solid yellowgreen;
      border-bottom-left-radius: 16px 14px;
      transform:translate(0, -2px);
    }
    
    &:after {
      content:"";
      position:absolute;
      z-index:1;
      bottom:-2px;
      right:-56px;
      width:26px;
      height:20px;
      background:white;
      border-bottom-left-radius: 10px;
      transform:translate(-30px, -2px);
    }
    ${props => props.contentType === THOUGHT_MESSAGE && css`
			color: grey
		`}
	`}
`;


export const MessageContainer = styled.div`
  display: flex;
  flex-flow: column;
  margin: 20px auto;
  max-width: 400px;
`;

export const AppContainer =  styled.div`
  text-align: center;
`;

export const Input = styled.input`
  padding: 10px;
  border: solid 5px #c9c9c9;
  font-size: 18px;
  width: 500px;
`;

export const Header = styled.h1`
  height: 50px;
`;
