const io = require('socket.io')();
const LOCAL_MESSAGE_TYPE = 'local';
const EXTERNAL_MESSAGE_TYPE = 'external';
const THOUGHT_MESSAGE = 'thought';

io.on('connection', function(socket){

  socket.on('add nickname', function (nick) {
    socket.broadcast.emit('nickname added', {
      nick,
    });
  });

  socket.on('chat message', function (content) {
    socket.broadcast.emit('chat message', {
      content,
      messageType: EXTERNAL_MESSAGE_TYPE
    });
    socket.emit('chat message', {
      content,
      messageType: LOCAL_MESSAGE_TYPE,
    });
  });

  socket.on('thought message', function (content) {
    socket.broadcast.emit('chat message', {
      content,
      messageType: EXTERNAL_MESSAGE_TYPE,
      contentType: THOUGHT_MESSAGE
    });
    socket.emit('chat message', {
      content,
      messageType: LOCAL_MESSAGE_TYPE,
      contentType: THOUGHT_MESSAGE
    });
  });


  socket.on('remove last message', function () {
    socket.broadcast.emit('remove last message', {
      type: EXTERNAL_MESSAGE_TYPE
    });
    socket.emit('remove last message', {
      type: LOCAL_MESSAGE_TYPE
    });
  });
});

const port = 8000;
io.listen(port);
console.log('listening on port ', port);