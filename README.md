# Chat   Application

## To get up and running

To install 

```
yarn
```


To run the client side

```
yarn start
```

To run the server side

```
node server
```

## How to use

 -  `/nick   <name>`   -   sets   your   name   for   the   chat
 -  `/think   <message>`   -   makes   the   text   appear   in   dark   grey,   instead   of   black
 -  `/oops`   -   removes   the   last   message   sent